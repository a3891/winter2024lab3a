public class Student{
	
	public String name;
	public int age;
	public int classes;
	
	public String presentMyself() {
		return "Hi, I'm " + name + ", I'm " + age + " years old. I'm currently taking " + classes + " classes.";
	}
	
	public int removeClass() {
		return classes-1;
	}
}