public class Application{
	public static void main(String[] args) {
		
		Student student1 = new Student();
        student1.name = "Artem";
        student1.age = 19;
        student1.classes = 4;

        Student student2 = new Student();
        student2.name = "Eric";
        student2.age = 25;
        student2.classes = 7;
		
		System.out.println("We currently have two students:");
		System.out.println(student1.name + " and " + student2.name);
		
		//Student.presentMyself(); - Incorrect
		
		System.out.println(student1.presentMyself());
		System.out.println(student2.presentMyself());
		
		Student[] section3 = new Student[3];
		section3[0] = student1;
		section3[1] = student2;
		System.out.println(section3[0].name);
		//System.out.println(section3[2].name); --> NullPointerException, since the object isn't created yet
		
		section3[2] = new Student();
		section3[2].name = "Ashley";
        section3[2].age = 18;
        section3[2].classes = 6;
		System.out.println(section3[2].name + " " + section3[2].age + " " + section3[2].classes);
	}
}